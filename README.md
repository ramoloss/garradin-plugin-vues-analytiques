# Plugin Vues analytiques pour Paheko

Plugin pour le logiciel de gestion d'association Paheko ( https://garradin.eu/ - https://fossil.kd2.org/garradin ).
Source : https://gitlab.com/ramoloss/garradin-plugin-vues-analytiques


Un plugin encore écrit à l'arrache pour rajouter quelques vues analytiques qui me sont utiles.

## Installation:
Télécharger l'archive du plugin depuis la [page des releases](https://gitlab.com/ramoloss/garradin-plugin-vues-analytiques/-/releases), et la placer dans le dossier plugins de votre installation Paheko.


## Fonctionnalités :
- Vue par mois : Sélectionner des recherches enregistrées, une plage temporelle, et hop, un tableau de cette recherche découpé par mois
- Vue synthétiques des projets : Seulement la ligne total des projets est affiché, avec les comptes de résultats, l'actif et le passif
- Rajoute les colonnes Résultat, Actif et Passif, sur les vues Par projets/Par exercice
- Diagramme de Sankey : parcours tout les projets du plan comptable actuel et en génère un diagramme de Sankey, grâce à SankeyMATIC
- Possibilité d'enregistrer le diagramme en SVG directement sur le serveur, dans un fichier spécifié. Soyez prudent avec cette fonction, elle peut permettre d'envoyer n'importe quel contenu sur le serveur, aucune vérification n'est faite. Soyez sûr·es de qui a accès à cette fonctionnalité.

Ce petit bazar est sous licence GPL 3


## Crédits 	:
- SankeyMATIC : https://github.com/nowthis/sankeymatic
---

Requête pour compte de résultat :
```
SELECT a.id, a.code, a.label, a.position, SUM(l.credit) AS credit, SUM(l.debit) AS debit,
			SUM(l.credit - l.debit) * 1.0 / 100  AS sum
			FROM acc_transactions_lines l
			INNER JOIN acc_transactions t ON t.id = l.id_transaction
			INNER JOIN acc_accounts a ON a.id = l.id_account
			WHERE ( "position" = 4  OR "position" = 5 ) AND a.type NOT IN (8) 
			GROUP BY l.id_account
			HAVING sum != 0
			ORDER BY "position", a.code COLLATE NOCASE;
```
