<?php
namespace Paheko;

$db = DB::getInstance();
$db->import(dirname(__FILE__) . "/data/schema.sql");
$config = new \StdClass;
$config->export_path = PLUGINS_ROOT.'/vues_analytiques/www/admin/sankey.svg';
$config->enable_export = false;
$plugin->setConfig($config);
