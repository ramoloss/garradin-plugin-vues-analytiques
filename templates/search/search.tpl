{include file="_head.tpl" title="%s — %s"|args:$s.label,$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

<nav class="tabs">
	<ul>
		<li class="current"><a href="{$self_url}">Search</a></li>
		<li><a href="list.php">Recherches enregistrées</a></li>
	</ul>
</nav>

{form_errors}

<form method="post" action="{$self_url}">
<fieldset>
	<legend>Plage d'affichage</legend>

	{input type="date" name="date_start" label="Date de début" required=1 source=$dates}
	{input type="date" name="date_end" label="Date de fin" required=1 source=$dates}

	<p class="submit">
		{csrf_field key="select_dates"}
		{button type="submit" name="select" label="Sélectionner" class="main" shape="right"}
		{button type="submit" name="save" label="Enregistrer pour cette recherche" class="main" shape="document"}
	</p>
</fieldset>
</form>

{if !empty($result)}

	<p class="help">{$result|count} écritures trouvées pour cette recherche.</p>
	<table class="list search">
		<thead>
			<tr>
				{foreach from=$result_header key="key" item="label"}
 					<td>{$label}</td>
 				{/foreach}
 			</tr>
		</thead>
		<tbody>
			{foreach from=$labels item="compte" key="code"}
			<?php $total = 0; ?>
			<?php $mean = 0; ?>
			<tr>
				{if $code == 'total'}<th>{else}<td>{/if}	
				{$code} - {$compte}
				{if $code == 'total'}</th>{else}</td>{/if}	
			
				{foreach from=$ordered item="month" key="k"}
					{if $code == 'total'}<th>{else}<td>{/if}	
						{if isset($month[$code])}
							{$month[$code]|raw|money}
							<?php $total += $month[$code]?>
							<?php $mean += $month[$code]?>
						{/if}
					{if $code == 'total'}</th>{else}</td>{/if}	
				{/foreach}
				{if $code == 'total'}<th>{else}<td>{/if}	
				{$total|raw|money}
				{if $code == 'total'}</th>{else}</td>{/if}	
				{if $code == 'total'}<th>{else}<td>{/if}
				<i>{$mean / $number_of_months|raw|money}</i>
				{if $code == 'total'}</th>{else}</td>{/if}
			</tr>
			{/foreach}
		</tbody>
</table>
	
{else}
	<p class="block alert">
		Aucun résultat trouvé.
	</p>
{/if}

{include file="_foot.tpl"}
