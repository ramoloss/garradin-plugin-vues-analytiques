<?php

namespace Paheko;

use Paheko\Accounting\Years;

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

$current_year_id = $session->get('acc_year');
define('Paheko\CURRENT_YEAR_ID', $current_year_id);
if ($current_year_id) {
	// Check that the year is still valid
	$current_year = Years::get($current_year_id);
}
if (!$current_year_id) {
	$current_year = Years::getCurrentOpenYear();
}
