<?php

namespace Paheko;
use Paheko\Users\Session;

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

require_once __DIR__ . '/_inc.php';

$access_section = CURRENT_SEARCH_TARGET == 'accounting' ? $session::SECTION_ACCOUNTING : $session::SECTION_USERS;


$tpl->assign('list', Search::list(CURRENT_SEARCH_TARGET, Session::getUserId()));
$mode = 'list';
$tpl->assign(compact('mode', 'access_section'));


$tpl->display(PLUGIN_ROOT . '/templates/search/list.tpl');
