<?php
namespace Paheko;

use Paheko\Entities\Search as SE;
use Paheko\Search;
use Paheko\Users\Session;
use Paheko\Plugin\vues_analytiques\Recherches;
use DateTime;
use DateInterval;
use DatePeriod;

require_once __DIR__ . '/_inc.php';

$db = DB::getInstance();
$id = f('id') ?: qg('id');
if(empty($id))
{
	Utils::redirect(PLUGIN_ADMIN_URL . 'search/list.php');
}

$start = $end = null;
$r = Recherches::get($id);
if ($r !== false)
{
	$start = DateTime::createFromFormat('d/m/Y', $r->date_start);
	$end = DateTime::createFromFormat('d/m/Y', $r->date_end);
}

// Formulaire de dates
$form->runIf(f('save') || f('select'), function () use ($id, &$start, &$end) {
	if (null === f('date_start') || null === f('date_end'))
	{
		throw new UserException('Date de début ou de fin non renseignée');
	}
        $start = DateTime::createFromFormat('d/m/Y', f('date_start'));
        $end = DateTime::createFromFormat('d/m/Y', f('date_end'));
	
	if (false === $start || false === $end)
	{
		throw new UserException('Date de début ou de fin non valide');
	}

	if (f('save')) {
		Recherches::save($id, [
			'date_start' => f('date_start'),
			'date_end' => f('date_end'),
		]);
	}
});

// Get search
// Copied from Paheko + filtered
$s = Search::get($id);
if (!$s) {
	throw new UserException('Recherche inconnue ou invalide');
}

$text_query = trim((string) qg('qt'));
$sql_query = trim((string) f('sql'));
$json_query = f('q') ? json_decode(f('q'), true) : null;
$default = false;

if ($text_query !== '') {
	$s->content = json_encode($s->getAdvancedSearch()->simple($text_query, true));
	$s->type = SE::TYPE_JSON;
}
elseif ($sql_query !== '') {
	// Only admins can run custom queries, others can only run saved queries
	$session->requireAccess($access_section, $session::ACCESS_ADMIN);

	if (Session::getInstance()->canAccess($access_section, Session::ACCESS_ADMIN) && f('unprotected')) {
		$s->type = SE::TYPE_SQL_UNPROTECTED;
	}
	else {
		$s->type = SE::TYPE_SQL;
	}

	$s->content = $sql_query;
}
elseif ($json_query !== null) {
	$s->content = json_encode(['groups' => $json_query]);
	$s->type = SE::TYPE_JSON;
}
elseif (!$s->content) {
	$s->content = json_encode($s->getAdvancedSearch()->defaults());
	$s->type = SE::TYPE_JSON;
	$default = true;
}

$list = $results = $header = $count = null;
/// End of get search


// Ajoute une condition de date si pas déjà présente
// May conflict with une condition de date qu'est pas "BETWEEN" j'imagine ? ça a bof l'air
if (!preg_match('("t"\."date" BETWEEN \'(?:\d{4}-\d{2}-\d{2}\') AND \'(?:\d{4}-\d{2}-\d{2}\'))', $s->content))
{
    $s->content = preg_replace('#WHERE#', 'WHERE ("t"."date" BETWEEN \'1970-01-01\' AND \'1970-01-01\') AND', $s->content);
}

// Si aucune dates n'ont été spécifiées (formulaire ou enregistrée), on en met par défaut
if (!isset($start, $end) || $start === false || $end === false)
{
    $start = new DateTime('3 month ago');
    $end = new DateTime('today');
}

// Effectuer la requête pour chaque mois
$interval = DateInterval::createFromDateString('1 month');
$period = new DatePeriod($start, $interval, $end);
$number_of_months = 0;

$labels = [];
foreach ($period as $month)
{
    $k = $month->format('m/Y');
    $dates[0] = $month->format('Y-m-01');
    $dates[1] = $month->format('Y-m-31');
    
    $s->content = preg_replace(
    '("t"\."date" BETWEEN \'(?:\d{4}-\d{2}-\d{2}\') AND \'(?:\d{4}-\d{2}-\d{2}\'))',
    '"t"."date" BETWEEN \''.$dates[0].'\' AND \''.$dates[1].'\'',
    $s->content);

    // Execute search
    $result[$k] = $s->query( ['no_cache' => true]);

    $ordered[$k] = [];
    $ordered[$k]['total'] = 0;
    while ($line = $result[$k]->fetchArray(\SQLITE3_ASSOC) )
    {
        if (!isset($line['code']) && isset($line['account_code']))
        {
            $line['code'] = $line['account_code'];
        }

        if (!isset($line['credit']))
        {
            $line['credit'] = 0;
        }
        elseif (!isset($line['debit']))
        {
            $line['debit'] = 0;
        }
        
        $montant = $line['credit'] - $line['debit'];
        $ordered[$k][$line['code']] = $montant;
        
        if (!array_key_exists($line['code'], $labels))
        {
            $labels[] = $line['code'];
        }

        $ordered[$k]['total'] += $montant;
    }
	$number_of_months++;
}

// To sort by code
$labels = $db->getAssoc('SELECT code, label FROM acc_accounts WHERE code in (\''.implode('\',\'', $labels).'\')');
$labels = $labels + ['total' => 'Total'];

if (!empty($labels))
{
    $header = array_keys($ordered);
    array_push($header, 'Total');
    array_push($header, 'Moyenne');
    array_unshift($header, 'Compte');
    $tpl->assign('result_header', $header);
}
else
{
    $result = null;
}

$dates['date_start'] = $start->format('Y-m-d');
$dates['date_end'] = $end->format('Y-m-d');
$tpl->assign(compact('s', 'list', 'header', 'result', 'dates', 'labels', 'ordered', 'number_of_months'));

$tpl->display(PLUGIN_ROOT . '/templates/search/search.tpl');
