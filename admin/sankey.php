<?php

namespace Paheko;

use Paheko\Accounting\Reports;
use Paheko\DB;
use Paheko\Entities\Accounting\Account;

require_once __DIR__ . '/_inc.php';

$plugin = new Plugin('vues_analytiques');
$db = DB::getInstance();
$enable_export = (bool) $plugin->getConfig('enable_export');

$text = '';
$processed = $sankey_data = $charges = $produits = [];


if (f('select'))
{
    $form->check('select_accounts', [
        'charges' => 'numeric',
        'produits' => 'numeric',
        ]);
    
    try
    {
        if (!$form->hasErrors())
        {
            $plugin->setConfig('id_charges', f('charges'));
            $plugin->setConfig('id_produits', f('produits'));
            $plugin->setConfig('group_charges', f('group_charges'));
            $plugin->setConfig('group_produits', f('group_produits'));
            $plugin->setConfig('extra_lines', f('extra_lines'));
            $plugin->setConfig('display_codes', f('display_codes'));
        }
    }
    catch (UserException $e)
    {
        $form->addError($e->getMessage());
    }
}
if (f('save') && $enable_export)
{
    $form->check('export_sankey', [
        'svg' => 'required',
    ]);
    
    try
    {
        if (!$form->hasErrors())
        {
            // Peut-être vérifier des trucs si on peut up n'importe quoi là jsp
            $filename = $plugin->getConfig('export_path');
            file_put_contents($filename, f('svg'));
        }
    }
    catch (UserException $e)
    {
        $form->addError($e->getMessage());
    }
}
    
// Code des comptes de charges et produits réfléchis
// A voir comment faire s'il y aurait plusieurs comptes pour produits/charges
$reflechi[0] = $plugin->getConfig('id_charges');
$reflechi[1] = $plugin->getConfig('id_produits');
$group['group_charges'] = $plugin->getConfig('group_charges');
$group['group_produits'] = $plugin->getConfig('group_produits');
$params['extra_lines'] = $plugin->getConfig('extra_lines');
$params['display_codes'] = $plugin->getConfig('display_codes');

$acc = $db->getAssoc('SELECT id, label FROM acc_projects ORDER BY label;');

// Pour chaque projet
foreach ($acc as $id => $label)
{
    $resultat = Reports::getStatement(['analytical' => $id,'exclude_type' => [Account::TYPE_VOLUNTEERING_REVENUE, Account::TYPE_VOLUNTEERING_EXPENSE]]);
    $code = $db->first('SELECT id, code FROM acc_accounts WHERE id = '.$id.';')->code;
    
    // Le résultat contient 'revenue, expense, revenue_sum, expense_sum, result'
    // Seulement revenue et expense nous intéressent
    foreach ($resultat as $type => $content)
    {
        if ($type != 'body_left' && $type != 'body_right')
        {
            continue;
        }
        
        $rev = $type == 'body_right' ? 1:0;
        
        // Pour chaque compte de résultat dans le projet
        foreach ($content as $k => $v)
        {

            // Pour les comptes réfléchis, parcourir chaque écriture et récupérer l'autre projet vers lequel elle pointe
            // Si le code du compte de résultat correspond à un compte réfléchi donné
            if ($v->id == $reflechi[$rev])
            {
                // On récupère toutes les numéros de transactions dans ce projet qui sont du compte réfléchi
                $sql = sprintf('SELECT l.id, id_transaction
                        FROM acc_transactions_lines l
                        INNER JOIN acc_accounts a ON a.id = l.id_account
                        LEFT JOIN acc_accounts b ON b.id = l.id_project
                        WHERE l.id_account = %d AND l.id_project = %d;', $v->id, $id);

                $lines = $db->get($sql);
                
                // Pour chaque transaction correspondant au projet + compte réfléchi...
                foreach ($lines as $line)
                {

                    // Transaction déjà traitée (comme ça va d'un projet à un autre, on les croisera 2 fois)
                    if (in_array($line->id_transaction, $processed))
                    {
                        continue;
                    }

                    // ... on récupère les lignes correspondantes, pour avoir le projet origine+cible
                    $sql = sprintf('SELECT l.id, id_transaction, credit, debit, b.label, b.code, b.id as id_project
                                    FROM acc_transactions_lines l
                                    LEFT JOIN acc_accounts b ON b.id = l.id_project
                                    WHERE id_transaction = %d AND id_account IN (%s);', $line->id_transaction, implode(',', $reflechi));

                    $transaction = $db->get($sql);

                    foreach ($transaction as $l)
                    {
                        // Euh ouais jsp, si c'est pour savoir dans quel sens ça va, quel projet est origine qui est cible
                        // Comment on fait si on tombe sur une transaction avec + que deux lignes ainsi ?
                        // On va dit qu'on fait pas pck mon fonctionnement perso c'est pas ça donc osef
                        if ($l->debit)
                        {
                            $tmp['debit']['label'] = $l->label;
                            $tmp['debit']['value'] = $l->debit;
                            $tmp['debit']['code'] = $l->code;
                        }
                        else
                        {
                            $tmp['credit']['label'] = $l->label;
                            $tmp['credit']['value'] = $l->credit;
                            $tmp['credit']['code'] = $l->code;
                        }
                    }

                    if ($tmp['debit']['value'] != $tmp['credit']['value'])
                    {
                        $form->addError('Ouais jsp il y a pas la même valeur sur deux lignes d\'une transaction dans un compte réfléchi dans le projet '. $label);
                        continue;
                    }

                    $sankey_data['source'][]['label'] = $tmp['debit']['label'];
                    $sankey_data['value'][] = $tmp['debit']['value'] / 100;
                    $sankey_data['target'][]['label'] = $tmp['credit']['label'];
                        
                    end($sankey_data['target']);
                    $last = key($sankey_data['target']); 
                    $sankey_data['source'][$last]['code'] = $tmp['debit']['code'];
                    $sankey_data['target'][$last]['code'] = $tmp['credit']['code'];
                    
                    $processed[] = $line->id_transaction;
                }
            }
            elseif ($rev)
            {
                $sankey_data['source'][]['label'] = $v->label;
                $sankey_data['value'][] = $v->balance / 100;
                $sankey_data['target'][]['label'] = $label;
                
                end($sankey_data['target']);
                $last = key($sankey_data['target']); 
                $sankey_data['source'][$last]['code'] = $v->code;
                $sankey_data['target'][$last]['code'] = $code;
                                
                if (strlen($v->code) > 3)
                {
                    $parent = substr($v->code, 0, 3);
                    $produits[$parent] = isset($produits[$parent]) ? $produits[$parent] + $v->balance : $v->balance;
                }
            }
            else
            {
                $sankey_data['source'][]['label'] = $label;
                $sankey_data['value'][] = $v->balance / 100;
                $sankey_data['target'][]['label'] = $v->label;
                
                end($sankey_data['source']);
                $last = key($sankey_data['source']);
                $sankey_data['target'][$last]['code'] = $v->code;                 
                $sankey_data['source'][$last]['code'] = $code;                 


                if (strlen($v->code) > 3)
                {
                    $parent = substr($v->code, 0, 3);
                    $charges[$parent] = isset($charges[$parent]) ? $charges[$parent] + $v->balance : $v->balance;
                }
            }
        }
    }

	// var_dump($resultat);die;
    $resultat->$id['label'] = $label;
}

// Get labels for grouped expenses/revenues
if ($group['group_charges'] || $group['group_produits'])
{
    $codes = array_merge(array_keys($charges), array_keys($produits));

    $sql = sprintf('SELECT code, label
                    FROM acc_accounts
                    WHERE code IN (%s);',
                    implode(',', $codes));

    $parent_labels = $db->getAssoc($sql);
}

// Regroupe les charges
if ($group['group_charges'])
{
    foreach ($charges as $code => $sum)
    {
        $charges[$code] /= 100;
    }

    foreach ($sankey_data['target'] as $i => $target)
    {
        $code = substr($target['code'], 0, 3);
        if (array_key_exists($code, $charges))
        {
            $print_label = '@'.$parent_labels[$code];

            $sankey_data['source'][]['label'] = $print_label;
            $sankey_data['value'][] = $sankey_data['value'][$i];
            $sankey_data['target'][] = $target;

            end($sankey_data['source']);
            $last = key($sankey_data['source']);
            $sankey_data['source'][$last]['code'] = $code;

            $sankey_data['target'][$i]['label'] = $print_label;
            $sankey_data['target'][$i]['code'] = $code;
        }
    }
}

// Regroupe les produits
if ($group['group_produits'])
{
    foreach ($produits as $code => $sum)
    {
        $produits[$code] /= 100;
    }
    
    foreach ($sankey_data['source'] as $i => $source)
    {
        $code = substr($source['code'], 0, 3);
        if (array_key_exists($code, $produits))
        {
            $print_label = '@'.$parent_labels[$code];

            $sankey_data['source'][] = $source;
            $sankey_data['value'][] = $sankey_data['value'][$i];
            $sankey_data['target'][]['label'] = ''.$print_label;
            
            end($sankey_data['target']);
            $last = key($sankey_data['target']);
            $sankey_data['target'][$last]['code'] = $code;
            

            $sankey_data['source'][$i]['label'] = $print_label;
            $sankey_data['source'][$i]['code'] = $code;
        }
    }
}


// Format text
foreach ($sankey_data['source'] as $i => $source)
{
    if ($params['display_codes'])
    {
        $text .= $sankey_data['source'][$i]['code']. ' - '.
        $sankey_data['source'][$i]['label'].' ['.
        $sankey_data['value'][$i]. '] '.
        $sankey_data['target'][$i]['code']. ' - '.
        $sankey_data['target'][$i]['label']. "\n";
    }
    else
    {
        $text .= $sankey_data['source'][$i]['label'].' ['.
        $sankey_data['value'][$i]. '] '.
        $sankey_data['target'][$i]['label']. "\n";
    }
}

if (!empty($params['extra_lines']))
{
    $text .= $params['extra_lines'];
}

$tpl->assign(compact('text', 'group', 'params', 'enable_export'));
$tpl->assign('plugin_css', ['sk/build.css']);
$tpl->assign('reflechi', ['charges' => $reflechi[0], 'produits' => $reflechi[1] ]);
$tpl->assign('chart_id', $current_year_id);
$tpl->display(PLUGIN_ROOT . '/templates/sankey.tpl');