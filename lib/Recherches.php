<?php

namespace Paheko\Plugin\vues_analytiques;

use Paheko\DB;
use Paheko\Utils;
use Paheko\UserException;

class Recherches
{
    const TABLE = 'plugin_analytique_recherches';

    public static function get($id)
    {
		$db = DB::getInstance();

		$r = $db->first('SELECT * FROM '.self::TABLE.' WHERE id = ? LIMIT 1;', (int) $id);

		return $r;
    }

    public static function save($id, $data = [])
    {
        $db = DB::getInstance();
        if ($db->test(self::TABLE, 'id = '. $id))
        {
            return $db->update(self::TABLE, $data, $db->where('id', (int) $id));
        }
		$db->insert(self::TABLE, ['id' => $id] + $data);
		return $db->lastInsertRowId();	
    }
}